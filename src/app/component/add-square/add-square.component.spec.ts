import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSquareComponent } from './add-square.component';

describe('AddSquareComponent', () => {
  let component: AddSquareComponent;
  let fixture: ComponentFixture<AddSquareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSquareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSquareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
