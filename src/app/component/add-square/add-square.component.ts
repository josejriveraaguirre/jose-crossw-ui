import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Square } from 'src/app/models/Square';
import { SquareService } from 'src/app/services/square.service';

@Component({
  selector: 'app-add-square',
  templateUrl: './add-square.component.html',
  styleUrls: ['./add-square.component.css']
})
export class AddSquareComponent implements OnInit {

  newSquare: Square = {
    cellId: "",
    letter: ""
};

  isEditing: boolean = false;

  constructor(private squareSvc: SquareService, 
    private router: Router, 
    private activeRoute: ActivatedRoute) {}

  ngOnInit(): void {

    var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");

    if (isIdPresent) {
      const id = this.activeRoute.snapshot.paramMap.get('id')
      this.squareSvc.viewSquare(Number(id)).subscribe(
        data => this.newSquare = data
      )
      this.isEditing = true;
    }
  } // end of ngOnInit

  savedSquare() {
    this.squareSvc.saveSquare(this.newSquare).subscribe(
      data => {
        this.router.navigateByUrl("/squares");
      }
    )
  }

  deletedSquare(id: number) {
    this.squareSvc.deleteSquare(id).subscribe(
      data => {
        this.router.navigateByUrl("/squares");
      }
    )
  }

} // end of class

