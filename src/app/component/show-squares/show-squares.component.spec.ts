import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSquaresComponent } from './show-squares.component';

describe('ShowSquaresComponent', () => {
  let component: ShowSquaresComponent;
  let fixture: ComponentFixture<ShowSquaresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowSquaresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSquaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
