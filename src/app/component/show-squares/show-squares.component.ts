import { Component, OnInit } from '@angular/core';
import { Square } from 'src/app/models/Square';
import { SquareService } from 'src/app/services/square.service';

@Component({
  selector: 'app-show-squares',
  templateUrl: './show-squares.component.html',
  styleUrls: ['./show-squares.component.css']
})
export class ShowSquaresComponent implements OnInit {

  squares: Square[] = [];

  constructor(private squareSvc: SquareService) { }

  ngOnInit(): void {

    this.listSquares();
    
  } //end of ngOnInit

  listSquares() {
    this.squareSvc.getSquares().subscribe(
      data => this.squares = data 
    )
  }

  deletedSquare(id: number) {
    this.squareSvc.deleteSquare(id).subscribe(
      data => this.listSquares()
    )
  }

} // end of class

