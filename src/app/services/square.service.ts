import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Square } from '../models/Square';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SquareService {

  private getUrl: string = "http://localhost:8080/api/v1/squares";

    constructor(private httpClient: HttpClient) { }

    getSquares(): Observable<Square[]> {
      return this.httpClient.get<Square[]>(this.getUrl).pipe(
        map (result => result)
      )
    }

    saveSquare(newSquare: Square): Observable<Square> {
      return this.httpClient.post<Square>(this.getUrl, newSquare);
    }

    viewSquare(id: number): Observable<Square> {
      return this.httpClient.get<Square>(`${this.getUrl}/${id}`).pipe(
        map(result => result)
      )
    }

    deleteSquare(id: number): Observable<any> {
      return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: "text"})
    }
    
} // end of class
