export interface Square {
    cellId: string,
    letter: string
}